// console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	let fistName = prompt("Enter your name?");

	console.log("Hello, " + fistName);

	let myAge = prompt("How old are you?");

	console.log("You are " + myAge);

	let myAddress = prompt("Where do you live?");

	console.log("You live in " + myAddress);

function showSampleAlert() {
		alert("Thank you for your input!");
	}

	showSampleAlert();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function bandNames() {
		console.log("1. Eraserheads")
		console.log("2. Beegees")
		console.log("3. Carpenters")
		console.log("4. Coldplay")
		console.log("5. Maroon 5")
	}
	bandNames();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function movieNames() {
		let movieOne = "A Man Called Otto"
		let movieTwo = "Palm Trees and Power Lines"
		let movieThree = "Everything Everywhere All at Once"
		let movieFour = "All That Breathes"
		let movieFive = "The Whale"
		

		console.log("1. " + movieOne);
			console.log("Rotten Tomatoes Rating: 69%");

		console.log("2. " + movieTwo);
			console.log("Rotten Tomatoes Rating: 90%");

		console.log("3. " + movieThree);
			console.log("Rotten Tomatoes Rating: 95%");

		console.log("4. " + movieFour);
			console.log("Rotten Tomatoes Rating: 99%");

		console.log("5. " + movieFive);
			console.log("Rotten Tomatoes Rating: 66%");
	
	}
	movieNames();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	alert("Hi! Please add the names of your friends.");

let printFriends = function printUsers(){
	
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
// console.log(friend1);
// console.log(friend2);
// console.log(friend3);